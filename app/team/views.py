from rest_framework import viewsets, mixins

from web.models import Team

from team.serializers import TeamSerializer


class TeamViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    queryset = Team.objects.all().order_by('name').order_by('-wins')
    serializer_class = TeamSerializer
    lookup_field = 'name'
