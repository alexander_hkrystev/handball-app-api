from rest_framework import serializers

from web.models import Team


class TeamSerializer(serializers.ModelSerializer):
    opponents = serializers.StringRelatedField(many=True)
    class Meta:
        model = Team
        fields = ('name', 'wins', 'opponents')
