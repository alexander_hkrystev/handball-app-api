from django.contrib import admin
from django.urls import path, include

from rest_framework.routers import DefaultRouter

from game.views import GameViewSet, UploadGameData, RunDataAnalysis
from team.views import TeamViewSet

router = DefaultRouter()
router.register('games', GameViewSet)
router.register('teams', TeamViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('upload-data/', UploadGameData.as_view(), name='upload-data'),
    path('analyse-data/', RunDataAnalysis.as_view(), name='analyse-data'),
    path('api/', include(router.urls)),

]
