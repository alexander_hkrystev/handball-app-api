from django.test import TestCase
from web import models


class TeamModelTests(TestCase):

    def test_create_team_object(self):
        """Create Team Object"""
        team = models.Team.objects.create(
            name='Bulgaria'
        )

        self.assertEqual(team.name, 'Bulgaria')


class GameModelTests(TestCase):

    def setUp(self) -> None:
        models.Team.objects.create(name='Bulgaria')
        models.Team.objects.create(name='Croatia')

    def test_create_game(self):
        """Create Game Object"""
        game = models.Game.objects.create(
            team1= models.Team.objects.get(name='Bulgaria'),
            team2= models.Team.objects.get(name='Croatia'),
            results=[[0,0],[0,2]]
        )
        self.assertEqual(game.team1.name, 'Bulgaria')
        self.assertEqual(game.results, [[0,0],[0,2]])

