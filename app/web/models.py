from django.db import models


class Team(models.Model):
    name = models.CharField(max_length=80)
    wins = models.IntegerField(default=0, blank=True)
    opponents = models.ManyToManyField('Team', related_name='team_opponents')

    def update_opponents(self, update_opponents_list, do_save=False):

        existing_opponents = [ opponent.name for opponent in self.opponents.all()]

        to_update = set(update_opponents_list) - set(existing_opponents)

        print(to_update)

        new_teams = Team.objects.filter(name__in=to_update)

        self.opponents.add(*new_teams)

        if do_save:
            self.save()





    def __str__(self):
        return self.name

class Game(models.Model):
    team1 = models.ForeignKey(Team, related_name='team1',on_delete=models.CASCADE)
    team2 = models.ForeignKey(Team, related_name='team2', on_delete=models.CASCADE)
    results = models.JSONField()

    def __str__(self):
        return f"{self.team1} - {self.team2}"
