from rest_framework.exceptions import ParseError
from rest_framework import viewsets, mixins
from rest_framework.views import APIView
from rest_framework.parsers import FileUploadParser, MultiPartParser
from rest_framework.response import Response
from rest_framework import status

from game.serializers import GameSerializer
from web.models import Game, Team
from game.tasks import update_stats




class UploadGameData(APIView):

    parser_classes = (MultiPartParser, )

    def put(self,request, format=None):

        if 'file' not in request.FILES:
            raise ParseError('No file content')

        for line in request.FILES['file']:
            line = line.strip().decode("utf-8")

            if line == 'stop':
                break

            game_arr = line.split(' | ')

            team1, created = Team.objects.get_or_create(name=game_arr[0])
            team2, created = Team.objects.get_or_create(name=game_arr[1])

            Game.objects.create(team1=team1,
                                team2=team2,
                                results=[game_arr[2].split(':'),game_arr[3].split(':')]
                                )

        return Response(status=status.HTTP_200_OK)


class RunDataAnalysis(APIView):

    def get(self, request):

        # Start celery task
        task = update_stats.delay()

        return Response({'task_id':task.id}, status=status.HTTP_200_OK)

class GameViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin):
    queryset = Game.objects.all()
    serializer_class = GameSerializer
