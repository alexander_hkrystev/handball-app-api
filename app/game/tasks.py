from app.celery_app import app as celery_app

from app import handbal_manager

from web import models


@celery_app.task()
def update_stats():

    handball_statistics = handbal_manager.HandballStatistics()

    for model_game in models.Game.objects.all():

        game_obj = handbal_manager.Game(
            team1_name=model_game.team1,
            team2_name=model_game.team2,
            result1=model_game.results[0],
            result2=model_game.results[1]

        )
        handball_statistics.update_stats(game_obj)

    for entry in handball_statistics.teams:
        team = models.Team.objects.get(name=entry.name)
        team.wins = entry.wins

        new_opponents = [o.name for o in entry.opponents]
        team.update_opponents(new_opponents)

        team.save()

