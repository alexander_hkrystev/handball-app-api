from rest_framework import serializers

from web.models import Game


class ResultsField(serializers.Field):

    def to_representation(self, value):
        return f"Match1:({value[0][0]}:{value[0][1]}); Match2:({value[1][0]}:{value[1][1]});"

class GameSerializer(serializers.ModelSerializer):
    team1 = serializers.StringRelatedField()
    team2 = serializers.StringRelatedField()
    results = ResultsField()
    class Meta:
        model = Game
        fields = ('team1', 'team2', 'results')

