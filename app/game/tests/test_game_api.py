import io

from django.urls import reverse

from rest_framework import status
from rest_framework.test import APIClient
from rest_framework.test import APITestCase


class GameUploadDataTests(APITestCase):

    def setUp(self):
        self.client = APIClient()

    def generate_data_file(self):
        file = io.BytesIO(b'Denmark | Belgium | 0:0 | 1:1\n'
                          b'Belgium | Austria | 2:0 | 0:2\n'
                          b'Latvia | Monaco | 2:0 | 0:0\n'
                          b'Bulgaria | Italy | 2:1 | 3:2\n'
                          b'stop\n')
        file.seek(0)

        return io.TextIOWrapper(file, encoding='utf-8')

    def test_upload_file(self):
        """Test upload file with games and create game objects"""

        data={'file': self.generate_data_file()}
        url = reverse('upload-data')

        response = self.client.put(url, data, format='multipart')

        self.assertEqual(response.status_code, status.HTTP_200_OK)

