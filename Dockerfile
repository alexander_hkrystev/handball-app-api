FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1

RUN apk add --update --no-cache postgresql-client
RUN apk add --update --no-cache --virtual .tmp-build-deps gcc libc-dev \
    linux-headers postgresql-dev

COPY ./wait-for /bin/wait-for
RUN chmod +x /bin/wait-for

COPY ./requirements.txt /requirements.txt

RUN pip install --no-cache-dir -r /requirements.txt

RUN apk del .tmp-build-deps

# Setup directory structure
RUN mkdir /app
WORKDIR /app
COPY ./app/ /app


